//
//  EntrantTournamentRecord.swift
//  PokerTournamentBuddy
//
//  Created by Justin Gazlay on 7/15/18.
//  Copyright © 2018 Justin Gazlay. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers class EntrantTournamentRecord: Object {
    dynamic var tournamentId: String = ""
    dynamic var id: String = ""
    dynamic var buyInAmount: Double = 0.00
    dynamic var addOns: Set<Double>? = nil
    dynamic var rebuys: Set<Double>? = nil
    dynamic var knockoutPosition: Int? = nil
    
    convenience init(tournamentId: String, id: String? = nil, buyInAmount: Double = 0.00) {
        self.init()
        self.tournamentId = tournamentId
        self.id = id ?? UUID().uuidString
        self.buyInAmount = buyInAmount
    }
    
    
    
}
