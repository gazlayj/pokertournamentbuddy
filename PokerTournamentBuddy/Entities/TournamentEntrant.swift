//
//  TournamentEntrant.swift
//  PokerTournamentBuddy
//
//  Created by Justin Gazlay on 7/15/18.
//  Copyright © 2018 Justin Gazlay. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers class TournamentEntrant: Object {
    dynamic var player = Player()
    dynamic var entrantRecord = EntrantTournamentRecord()
    
    convenience init(player: Player, entrantRecord: EntrantTournamentRecord) {
        self.init()
        self.player = player
        self.entrantRecord = entrantRecord
    }
}
