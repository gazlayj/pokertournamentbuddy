//
//  Player.swift
//  PokerTournamentBuddy
//
//  Created by Justin Gazlay on 7/15/18.
//  Copyright © 2018 Justin Gazlay. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers class Player: Object {
    dynamic var firstName: String = ""
    dynamic var lastName: String? = nil
    dynamic var id: String = ""
    dynamic var entries = LinkingObjects(fromType: TournamentEntrant.self, property: "player")
    
    convenience init(firstName: String, lastName: String? = nil, id: String? = nil) {
        self.init()
        self.firstName = firstName
        self.lastName = lastName
        self.id = id ?? UUID().uuidString
    }
    
    
}
